// Task 1

let number;
let numberCheck;

do {
  number = +prompt("Enter a number, please", [number]);
  numberCheck = Number.isInteger(number);
} while (isNaN(number) || numberCheck == false);

for (let i = 0; i <= number; i++) {
  if (i % 5 == 0) {
    console.log(i);
  } else if (i % 5 != 0) {
    continue;
  } else {
    console.log("Sorry, no numbers");
  }
}

// Task 2 (additional)

let primeCheck;
let stop;

let numberM;
let numberN;

do {
  do {
    numberM = +prompt("Enter a number M, please", [numberM]);
    numberCheck = Number.isInteger(numberM);
  } while (isNaN(numberM) || numberCheck == false);

  do {
    numberN = +prompt("Enter a number N, please", [numberN]);
    numberCheck = Number.isInteger(numberN);
  } while (isNaN(numberN) || numberCheck == false);

  if (numberM > numberN) {
    alert("Error! Wrong input, try again");
  }
} while (numberM > numberN);

for (let i = numberM; i <= numberN; i++) {
  stop = 0;
  for(let j = 2; j < i; j++){
    if(i % j == 0){
      stop = 1;
      break;
    }
  }
  if(i > 1 && stop == 0){
    console.log(i);
  }
}